<?php

namespace Dockerous\Pagespeed;

class Init {

    private $scripts = array();
    private $styles = array();

    function __construct() {
        if(!is_admin()){
            add_filter('style_loader_tag', array($this, 'rewrite_styles'), 10, 4);
            add_filter('script_loader_tag', array($this, 'rewrite_scripts'), 10, 3);
            //add_action('wp_enqueue_scripts',array($this,'add_script'));
            add_action('wp_head', array($this, 'print_styles'), 10);
            add_action('wp_head', array($this, 'header_scripts'), 10);
            add_action('wp_footer', array($this, 'footer_scripts'), 30);
        }
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');
        add_action('init', array($this, 'disable_embed'));
    }

    function disable_embed() {
        if (!is_admin()) {
            wp_deregister_script('wp-embed');
        }
    }

    function rewrite_styles($tag, $handle, $href = NULL, $media = NULL) {
        if (strpos($tag, "rel='alternate stylesheet'")) {
            return $tag;
        }
        $this->styles[] = $tag;
        $newtag1 = str_replace("rel='stylesheet'", "rel='preload'", $tag);
        $newtag2 = str_replace('/>', ' as="style" onload="this.rel=\'stylesheet\'" />', $newtag1);
        return $newtag2;
    }

    function print_styles() {
        echo "<noscript>";
        foreach ($this->styles as $style) {
            echo $style;
        }
        echo "</noscript>";
        ?>
        <script>
            !function(a){"use strict"; var b = function(b, c, d){function j(a){return e.body?a():void setTimeout(function(){j(a)})}function l(){f.addEventListener && f.removeEventListener("load", l), f.media = d || "all"}var g, e = a.document, f = e.createElement("link"); if (c)g = c;  else{var h = (e.body || e.getElementsByTagName("head")[0]).childNodes; g = h[h.length - 1]}var i = e.styleSheets; f.rel = "stylesheet", f.href = b, f.media = "only x", j(function(){g.parentNode.insertBefore(f, c?g:g.nextSibling)}); var k = function(a){for (var b = f.href, c = i.length; c--; )if (i[c].href === b)return a(); setTimeout(function(){k(a)})}; return f.addEventListener && f.addEventListener("load", l), f.onloadcssdefined = k, k(l), f}; "undefined" != typeof exports?exports.loadCSS = b:a.loadCSS = b}("undefined" != typeof global?global:this), function(a){if (a.loadCSS){var b = loadCSS.relpreload = {}; if (b.support = function(){try{return a.document.createElement("link").relList.supports("preload")} catch (a){return!1}}, b.poly = function(){for (var b = a.document.getElementsByTagName("link"), c = 0; c < b.length; c++){var d = b[c]; "preload" === d.rel && "style" === d.getAttribute("as") && (a.loadCSS(d.href, d, d.getAttribute("media")), d.rel = null)}}, !b.support()){b.poly(); var c = a.setInterval(b.poly, 300); a.addEventListener && a.addEventListener("load", function(){b.poly(), a.clearInterval(c)}), a.attachEvent && a.attachEvent("onload", function(){a.clearInterval(c)})}}}(this);</script>
        <?php
    }

    function rewrite_scripts($tag, $handle, $src) {
        $this->scripts[] = $src;
        return '';
        //return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }

    function add_script() {
        wp_enqueue_script('dockerous-pagespeed-loadcss', plugins_url('js/loadcss.js', __FILE__));
    }

    function header_scripts() {
        ?><script>function dsp(e, t, r){function n(){for (; d[0] && "loaded" == d[0][f]; )c = d.shift(), c[o] = !i.parentNode.insertBefore(c, i)}for (var s, a, c, d = [], i = e.scripts[0], o = "onreadystatechange", f = "readyState"; s = r.shift(); )a = e.createElement(t), "async"in i?(a.async = !1, e.head.appendChild(a)):i[f]?(d.push(a), a[o] = n):e.write("<" + t + ' src="' + s + '" defer></' + t + ">"), a.src = s} <?php
        $this->print_scripts();
        echo "</script>";
        $this->scripts = array();
    }

    function print_scripts() {
        if (empty($this->scripts)) {
            echo "<!-- No Scripts Exist -->";
            return;
        }
        $do_scripts = 'dsp(document,"script",[';
        for ($i = 0; $i < count($this->scripts); $i++) {
            $script_clean = explode('?ver', $this->scripts[$i]);
            $do_scripts .= '"' . $script_clean[0] . '",';
        }
        $do_scripts = rtrim($do_scripts, ",");
        $do_scripts .= ']);';
        echo $do_scripts;
    }

    function footer_scripts() {
        echo "<script>";
        $this->print_scripts();
        echo "</script>";
        $this->scripts = array();
    }

}

return new Init();
