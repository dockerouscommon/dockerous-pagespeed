<?php

class DockerousActivationManager {

    private $file;
    private $file_long;
    private $config;
    private $prefix;
    private static $active_plugins = False;
    private $active = False;
    /**
     * Constructor.
     */
    public function __construct($file,$file_long,$config) {
        $this->file = $file;
        $this->file_long = $file_long;
        $this->config = $config;
        $this->prefix = $this->config['dwp_prefix'];
        register_activation_hook( $this->file, array( $this , 'activate' ) );
        register_deactivation_hook( $this->file, array( $this , 'deactivate' ) );
        if(self::$active_plugins === FALSE){
            self::$active_plugins = get_option('dwp_activated_plugins',array());
        }
        if(in_array($this->config['plugin_name'], self::$active_plugins)){
            $this->active = TRUE;
        }
    }

    public function is_active(){
        return $this->active;
    }

    /**
     * Attempts to activate the plugin if at least PHP 5.4.
     */
    public function activate() {
        // Check PHP Version and deactivate & die if it doesn't meet minimum requirements.
        if(is_array($this->config) && key_exists('required', $this->config) && is_array($this->config['required'])){
            foreach($this->config['required'] as $v){
                switch ($v){
                    case 'dwp':
                        $this->verify_dwp();
                        break;
                    case 'php':
                        $this->check_version('php',PHP_VERSION,'PHP');
                        break;
                    case 'mysql':
                        global $wpdb;
                        $this->check_version('mysql',$wpdb->db_version(),'Mysql');
                        break;
                    case 'wp':
                        global $wp_version;
                        $this->check_version('wp',$wp_version,'Wordpress');
                        break;
                }
            }
        }
        self::$active_plugins[] = $this->config['plugin_name'];
        update_option('dwp_activated_plugins',self::$active_plugins);
        do_action($this->prefix."_activate");
    }

    public function verify_dwp(){
        if(!defined('DOCKEROUS_WPPLUGIN_VERSION')){
            $this->kill($this->config['plugin_name'].' requires '.DOCKEROUS_WPPLUGIN_NAME.'.');
        }
        $this->check_version('dwp',DOCKEROUS_WPPLUGIN_VERSION,DOCKEROUS_WPPLUGIN_NAME);
    }

    public function check_version($type,$value,$label = ''){
        if(empty($label)){
            $label = $type;
        }
        $checks = array('<', 'lt', '<=', 'le', '>', 'gt', '>=', 'ge', '==', '=', 'eq', '!=', '<>', 'ne');
        if(is_array($this->config)){
            if(key_exists($type."_version_check", $this->config)){
                if(!in_array($this->config[$type."_version_check"], $checks)){
                    $this->kill('Failed to complete check for required '.$label.' version');
                }
                if(key_exists($type."_version", $this->config)){
                    if(!version_compare($value,$this->config[$type."_version"],$this->config[$type."_version_check"])){
                        $this->kill($this->config['plugin_name'].' requires a version of '.$label.' '.$this->config[$type."_version_check"].' '.
                                $this->config[$type."_version"].'.');
                    }
                }else{
                    $this->kill($this->config['plugin_name'].' does not list a required '.$label.' version');
                }
            }else{
                $this->kill('Failed to complete check for required '.$label.' version');
            }
        }else{
            $this->kill($this->config['plugin_name'].' has an invalid config file');
        }
    }

    public function deactivate(){
        if(($key = array_search($this->config['plugin_name'], self::$active_plugins)) !== false) {
            unset(self::$active_plugins[$key]);
        }
        update_option('dwp_activated_plugins',self::$active_plugins);
        do_action($this->prefix."_deactivate");
    }

    private function kill($message){
        deactivate_plugins( $this->file_long );
        wp_die( __( $message, 'dockerous' ) );
    }
}
