<?php

return array(
    "plugin_name" => "Dockerous Pagespeed",
    "required" => array("php",'wp'),
    "wp_version" => "4.1",
    "wp_version_check" => ">=",
    "php_version" => "5.4",
    "php_version_check" => ">=",
    "dwp_prefix" => "dps",
);