<?php
/**
 * Plugin Name: Dockerous Pagespeed
 * Description: Wordpress Plugin to increase pagespeed
 * Version:0.1
 * Author: Dockerous
 * License: GPLV3
 * License URI: https://www.gnu.org/licenses/gpl.html
 */




if(!class_exists("DockerousActivationManager")){
    require_once 'activate.php';
}

if(!class_exists('DockerousPagespeed')){
    class DockerousPagespeed{
        public $activate;
        public $config;
        public $init;
        
        function __construct() {
            global $dockerous_plugins;
            if(!is_array($dockerous_plugins)){
                $dockerous_plugins = array();
            }
            $dockerous_plugins[] = dirname(__FILE__);
            $this->config = require 'config.php';
            $this->activate = new DockerousActivationManager(__FILE__,plugin_basename( __FILE__ ),$this->config);
            add_action('plugins_loaded', array($this, 'load'), 20);
        }
        
        function load(){
            if(!class_exists('DockerousAutoloader')){
                require_once 'autoloader.php';
            }
            $this->init = require_once 'init.php';
        }
    }
}
$dockerous_pagespeed = new DockerousPagespeed();